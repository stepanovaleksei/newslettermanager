import logging

from django.views.decorators.csrf import csrf_exempt
from drf_yasg.utils import swagger_auto_schema
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Client, Message, Newsletter
from .serializers import ClientSerializer, NewsletterSerializer
from .tasks import run_active_newsletters

logging.basicConfig(
    filename="app.log",
    filemode="w",
    format=' format="%(asctime)s - %(levelname)s - %(message)s',
    level=logging.DEBUG,
)


class NewsletterInfoView(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        operation_description="Метод получения информации о рассылках и сообщениях"
    )
    def get(self) -> Response:
        newsletters_count = Newsletter.objects.all().count()
        messages_count = Message.objects.all().count()
        messages_delivered = Message.objects.filter(status=True).count()
        messages_not_delivered = Message.objects.filter(status=False).count()
        logging.debug("Newsletters has been counted")
        return Response(
            {
                "Amount of newsletters": newsletters_count,
                "Amount of messages": messages_count,
                "Messages Delivered": messages_delivered,
                "Messages Not Delivered": messages_not_delivered,
            }
        )


class NewsletterInfo2View(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        operation_description="Метод получения информации о статусе сообщений по выбранной рассылке"
    )
    def get(self, pk) -> Response:
        newsletter = get_object_or_404(Newsletter.objects.all(), pk=pk)
        messages_delivered = (
            Message.objects.filter(newsletter=newsletter).filter(status=True).count()
        )
        messages_not_delivered = (
            Message.objects.filter(newsletter=newsletter).filter(status=False).count()
        )
        logging.debug("Messages has been counted")
        return Response(
            {
                "Messages Delivered for selected nl": messages_delivered,
                "Messages Not Delivered for selected nl": messages_not_delivered,
            }
        )


class NewslettersView(APIView):
    permission_classes = (AllowAny,)

    def get(self) -> Response:
        newsletters = Newsletter.objects.all()
        serializer = NewsletterSerializer(newsletters, many=True)
        logging.debug("Newsletters has been viewed")
        return Response({"newsletters": serializer.data})

    @swagger_auto_schema(request_body=NewsletterSerializer)
    def post(self, request) -> Response:
        newsletter = request.data.get("newsletter")
        serializer = NewsletterSerializer(data=newsletter)
        if serializer.is_valid(raise_exception=True):
            newsletter_saved = serializer.save()
            logging.debug("A newsletter has been created")
            return Response(
                {
                    "success": "Newsletter '{}' created successfully".format(
                        newsletter_saved.start_datetime
                    )
                }
            )


class NewsletterView(APIView):
    permission_classes = (AllowAny,)

    def delete(self, pk) -> Response:
        newsletter = get_object_or_404(Newsletter.objects.all(), pk=pk)
        newsletter.delete()
        logging.debug(f" newsletter  {pk} was deleted")
        return Response(
            {"message": "Newsletter with id `{}` has been deleted.".format(pk)},
            status=204,
        )

    @swagger_auto_schema(request_body=NewsletterSerializer)
    def put(self, request, pk) -> Response:
        saved_newsletter = get_object_or_404(Newsletter.objects.all(), pk=pk)
        data = request.data.get("newsletter")
        serializer = NewsletterSerializer(
            instance=saved_newsletter, data=data, partial=True
        )
        if serializer.is_valid(raise_exception=True):
            newsletter_saved = serializer.save()
            logging.debug(f" Newsletter  {pk} was updated")
            return Response(
                {
                    "success": "Newsletter '{}' updated successfully".format(
                        newsletter_saved.start_datetime
                    )
                }
            )


class ClientsView(APIView):
    permission_classes = (AllowAny,)

    @csrf_exempt
    @swagger_auto_schema(operation_description="Метод получения списка клиентов")
    def get(self) -> Response:
        clients = Client.objects.all()
        serializer = ClientSerializer(clients, many=True)
        logging.debug("Clients has been viewed")
        return Response({"clients": serializer.data})

    @csrf_exempt
    @swagger_auto_schema(
        request_body=ClientSerializer, operation_description="Метод создания клиента"
    )
    def post(self, request) -> Response:
        client = request.data.get("client")
        serializer = ClientSerializer(data=client)
        if serializer.is_valid(raise_exception=True):
            client_saved = serializer.save()
            logging.debug("A client has been created")
            return Response(
                {"success": "Client '{}' created successfully".format(client_saved.tel)}
            )
        else:
            return Response(
                {"unsuccessful": "Client is not created"}
            )


class ClientView(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(
        request_body=ClientSerializer,
        operation_description="Метод редактирования данных клиента",
    )
    def put(self, request, pk) -> Response:
        saved_client = get_object_or_404(Client.objects.all(), pk=pk)
        data = request.data.get("client")
        serializer = ClientSerializer(instance=saved_client, data=data, partial=True)
        if serializer.is_valid(raise_exception=True):
            client_saved = serializer.save()
            logging.debug(f" Client  {pk} was updated")
            return Response(
                {"success": "Client '{}' updated successfully".format(client_saved.tel)}
            )
        else:
            return Response(
                {"unsuccessful": "Client is not updated"}
            )

    @swagger_auto_schema(operation_description="Метод удаления клиента")
    def delete(self, pk) -> Response:
        client = get_object_or_404(Client.objects.all(), pk=pk)
        client.delete()
        logging.debug(f" Client  {pk} was deleted")
        return Response(
            {"message": "Client with id `{}` has been deleted.".format(pk)}, status=204
        )


class NewsletterRunView(APIView):
    permission_classes = (AllowAny,)

    @swagger_auto_schema(operation_description="Метод обработки активных рассылок")
    def get(self) -> Response:
        message = run_active_newsletters()
        return Response({"Message": message})
