import requests
import json
from dotenv import load_dotenv
from .models import Client, Newsletter, Message
import os

from main.celery import app
from datetime import datetime

import logging
from celery.utils.log import get_task_logger

from django.utils import timezone

load_dotenv()
AUTH_TOKEN = os.getenv("AUTH_TOKEN")
URL = os.getenv("URL")

head = {
    "accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "Bearer " + AUTH_TOKEN,
}

logger = get_task_logger(__name__)
logger.setLevel(logging.INFO)


@app.task
def send_message(client_id, client_tel, text) -> None:
    logger.info("Task started")

    data = {"id": client_id, "phone": client_tel, "text": text}
    requests.post(URL + str(client_id), headers=head, data=json.dumps(data))


@app.task
def run_active_newsletters() -> str:
    newsletters = Newsletter.objects.all()
    for newsletter in newsletters:
        curr_time = timezone.now()
        if newsletter.start_datetime < curr_time < newsletter.end_datetime:
            clients = Client.objects.all()
            for client in clients:
                if client.tag == newsletter.client_filter:
                    message_time = datetime.now()
                    try:
                        send_message.delay(client.id, client.tel, newsletter.text)
                        Message.objects.create(
                            created_at=message_time,
                            status=True,
                            newsletter=newsletter,
                            client=client,
                        )
                    except Exception as e:
                        logger.warning(e)
                        Message.objects.create(
                            created_at=message_time,
                            status=False,
                            newsletter=newsletter,
                            client=client,
                        )
            logger.info("Newsletter was sent")
            return "Newsletter was sent"
        else:
            logger.info("No Active Newsletters")
            return "No Active Newsletters"
