from django.db import models
from django.core.validators import MinLengthValidator


class Client(models.Model):
    tel = models.CharField(validators=[MinLengthValidator(11)], max_length=11)
    tel_code = models.CharField(max_length=255)
    tag = models.CharField(max_length=255)
    timezone = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"


class Message(models.Model):
    created_at = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=False)
    newsletter = models.ForeignKey(
        "Newsletter", related_name="newsletters", on_delete=models.CASCADE
    )
    client = models.ForeignKey(
        "Client", related_name="clients", on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"


class Newsletter(models.Model):
    start_datetime = models.DateTimeField(auto_now=True)
    text = models.TextField()
    client_filter = models.CharField(max_length=255)
    end_datetime = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"
