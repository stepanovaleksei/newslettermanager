from django.core.validators import MinLengthValidator
from rest_framework import serializers

from .models import Client, Newsletter


class NewsletterSerializer(serializers.ModelSerializer):
    start_datetime = serializers.DateTimeField()
    text = serializers.CharField(max_length=255)
    client_filter = serializers.CharField(max_length=255)
    end_datetime = serializers.DateTimeField()

    def update(self, instance, validated_data):
        instance.start_datetime = validated_data.get(
            "start_datetime", instance.start_datetime
        )
        instance.text = validated_data.get("text", instance.text)
        instance.client_filter = validated_data.get(
            "client_filter", instance.client_filter
        )
        instance.end_datetime = validated_data.get(
            "end_datetime", instance.end_datetime
        )
        instance.save()
        return instance

    class Meta:
        model = Newsletter
        fields = ("start_datetime", "text", "client_filter", "end_datetime")


class ClientSerializer(serializers.ModelSerializer):
    tel = serializers.CharField(max_length=255, validators=[MinLengthValidator(11)])
    tel_code = serializers.CharField(max_length=255)
    tag = serializers.CharField(max_length=255)
    timezone = serializers.CharField(max_length=255)

    def update(self, instance, validated_data):
        instance.tel = validated_data.get("tel", instance.tel)
        instance.tel_code = validated_data.get("tel_code", instance.tel_code)
        instance.tag = validated_data.get("tag", instance.tag)
        instance.timezone = validated_data.get("timezone", instance.timezone)
        instance.save()
        return instance

    class Meta:
        model = Client
        fields = ("tel", "tel_code", "tag", "timezone")
