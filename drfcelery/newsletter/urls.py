from django.urls import path
from .views import (
    NewsletterView,
    ClientView,
    NewsletterInfoView,
    NewslettersView,
    ClientsView,
    NewsletterInfo2View,
    NewsletterRunView,
)

app_name = "newsletter"

urlpatterns = [
    path("newsletters/", NewslettersView.as_view()),
    path("newsletters/<int:pk>", NewsletterView.as_view()),
    path("clients/", ClientsView.as_view()),
    path("clients/<int:pk>", ClientView.as_view()),
    path("newsletters-info/", NewsletterInfoView.as_view()),
    path("newsletters-info/<int:pk>", NewsletterInfo2View.as_view()),
    path("run-newsletters", NewsletterRunView.as_view()),
]
