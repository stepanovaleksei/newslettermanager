from django.contrib import admin

from .models import Message, Client, Newsletter

admin.site.register(Message)
admin.site.register(Client)
admin.site.register(Newsletter)
