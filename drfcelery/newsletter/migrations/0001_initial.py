# Generated by Django 4.1.3 on 2022-11-15 12:10

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tel', models.IntegerField(validators=[django.core.validators.MinLengthValidator(11)])),
                ('tel_code', models.CharField(max_length=255)),
                ('tag', models.CharField(max_length=255)),
                ('timezone', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Newsletter',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_datetime', models.DateTimeField()),
                ('text', models.TextField()),
                ('client_filter', models.CharField(max_length=255)),
                ('end_datetime', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField()),
                ('status', models.BooleanField(default=False)),
                ('cleint_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='clients', to='newsletter.client')),
                ('newsletter_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='newsletters', to='newsletter.newsletter')),
            ],
        ),
    ]
