FROM python:3.9.6-alpine

ENV PATH="/scripts:${PATH}"

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN pip3 install --upgrade pip
COPY ./requirements.txt requirements.txt
RUN apk add --update --no-cache --virtual .tmp gcc libc-dev linux-headers libffi-dev python3-dev libpq musl-dev
RUN pip3 install -r requirements.txt

RUN apk del .tmp

WORKDIR /usr/src/app

COPY . .

CMD ["entrypoint.sh"]
